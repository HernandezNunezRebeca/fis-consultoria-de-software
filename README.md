## La consultoria.  
```plantuml
@startmindmap
+[#F68AD3] La consultoria. 
**_ .
+++[#F6D68A] La consultoria nace porque existen grandes organizaciones \n que son los clientes de las consultoras.  
++++[#EFF68A] Servicios que presta.
*****_ son
++++++[#F68AD3] Reingenieria de procesos
*******_ .
++++++++[#F6D68A] Ayuda a las empresas a que organicen sus actividades, \n siguiendo unesquema de procesos para que sea mas optimo al que y manejan \n atendiendo los cambios.
++++++[#F68AD3] Gobierno TI
*******_ .
++++++++[#F6D68A] Esta ligado a un conjunto de mejores practicas.
*********_ que son
++++++++++[#EFF68A] Calidad
++++++++++[#EFF68A] Metodologia del desarrollo.
++++++++++[#EFF68A] Arquitectura empresarial.
++++++[#F68AD3] Oficina de proyectos
++++++[#F68AD3] Analitica avanzada de datos.
++++[#EFF68A] Servicios de estrategia.
*****_ .
++++++[#F68AD3] Definicion modelos \n organizativos y operativos.
++++++[#F68AD3] Definición planes/ modelos \n de negocio. 
++++++[#F68AD3] Definición de estrategia corporativa, \n comercial/ tecnologica.
++++++[#F68AD3] Segmentación de mercado.
++++++[#F68AD3] Definición de productos \n y servicios.
++++++[#F68AD3] Definición de planes operativos por áreas.
*******_ son
++++++++[#EFF68A] MK.
++++++++[#EFF68A] Ventas.
++++++++[#EFF68A] Atención al cliente.
++++++[#F68AD3] Planes de medios.
++++++[#F68AD3] Segmentación de clientes.
++++++[#F68AD3] Fusiones, adquisiciones y alianzas.
++++++[#F68AD3] Gestion de camapañas.
++++[#EFF68A] ¿Porqué es una profesión de futuro?
*****_ .
++++++[#F68AD3] En la sociedad del conocimiento, la consultoría, \n es un modo de vida, que permite alcanzar \n la felicidad profecional.
++++[#EFF68A] ¿Qúe exige la consultoría?
*****_ .
++++++[#F68AD3] Aptitud.
++++++[#F68AD3] Proactividad.
++++++[#F68AD3] Moluntad de mejorar.
++++++[#F68AD3] Responsabilidad.
++++++[#F68AD3] Compromiso.
@endmindmap
```

## Consultoria de software.  
```plantuml
@startmindmap
+[#E0A893] C\no\nn\ns\nu\nl\nt\no\nr\ni\na \n\nD\ne \n\nS\no\nf\nt\nw\na\nr\ne.
**_ .
+++[#8AF6D5] Desarrollo de software.
****_ .
+++++[#A7E79E] El desarrollo de software comienza cuando el \n cliente tiene una necasidad en su negocio. 
******_ .
+++++++[#E0A893] Ventajas.
********_ .
+++++++++[#8AF6D5] Agilidad y productividad. 
+++++++[#E0A893] Consultoría con el cliente.
********_ .
+++++++++[#8AF6D5] Requisitos del cliente.
+++++++[#E0A893] Estudio de viabilidad.
********_ .
+++++++++[#8AF6D5] Analizar lo que el cliente quiere.
+++++++++[#8AF6D5] Analizar ventajas y ahorro economico que el cliente tendra con el software
+++++++++[#8AF6D5] Verificar el coste que te llevara al desarrollo del software.
+++++++++[#8AF6D5] Verificar que el proyecto es viable.
+++++++[#E0A893] Diseño funcional.
********_ en que consiste 
+++++++++[#8AF6D5] Consiste en saber que información necesita el sistema, \n  de entrada en el sistema, que informacion se va a insertar\n en el sistema y cual es la información que el sistema arroja.
+++++++++[#8AF6D5] Construye el modelo de datos que se va a tener.
+++++++++[#8AF6D5] Cunatos procesos de entrada, salida y repetitivos \n va a tener ese sistema.
+++++++++[#8AF6D5] Se realiza un prototipo para que el cliente lo vea.
+++++++[#E0A893] Base de datos.
********_ .
+++++++++[#8AF6D5] La información de entrada y  salida \n es la que se almacena en la base de datos.
+++++++[#E0A893] Diseño tecnico.
********_ .
+++++++++[#8AF6D5] Lo que era una necesidad se traslada a un lenguaje de \n programación.
+++++++++[#8AF6D5] Crea todos los procesos y crea todas las bases de datos.
+++++++++[#8AF6D5] Prueba integrada
+++++++[#E0A893] Analistas programadores. 
********_ .
+++++++++[#8AF6D5] Se encargan de construir el software con el \n lenguaje que el diseñador tecnico decide.
+++++++++[#8AF6D5] Realizan pruebas.
+++++++[#E0A893] Prueba final. 
********_ .
+++++++++[#8AF6D5] El cliente lo prueba para que se de cuenta si el software \n cumple con todos los requerimientos.
+++++++++[#8AF6D5] Una vez que el cliente lo aprueba se instala en todos \n los ordenadores de la empresa.
+++[#8AF6D5] Actividades desarrolladoras en AXPE Consulting.
****_ son.
+++++[#A7E79E] Realizan todas las tareas que permiten crear o mantener\n la información de una empresa.
******_ .
+++++++[#E0A893] ¿Qúe necesita un empresa en su plan informatico\n para comenzar a operar y mantenerce?
********_ .
+++++++++[#8AF6D5] Tener una infrestructura.
+++++++++[#8AF6D5] Habilitar toda la infrestructura de comunicacion, que \n permite comunicarse con el exterior.
+++++++++[#8AF6D5] Dotarla de todo aquello que permita mantener segura\n la información que se almacena.
+++++++++[#8AF6D5] Aplicaciones que permitan desarrollar el negocio.
+++++++[#E0A893] Una vez que la infrestructura esta lista, ¿Como consegimos que siga funcionando?
********_ .
+++++++++[#8AF6D5] Monitoriar los procesos.
+++++++++[#8AF6D5] Verificar que las aplicaciones que tienen se adecuen a\n los cambios en el mercado.
+++++++[#E0A893] Factor de escala.
********_ .
+++++++++[#8AF6D5] La organizacion se adecua a la necesidad del servios,\n del proyecto y de el cliente.
+++++++++[#8AF6D5] Se tienen oficinas de proyectos que se encargan de\n las grandes empresas.
+++++++++[#8AF6D5] Con una empresa pequeña se ve de manera mas personalizada\n con equipos mas pequeño.
+++++++[#E0A893] Tendencias en el desarrollo de servicios.
********_ .
+++++++++[#8AF6D5] Empiezan por cambiar el concepto de lo que antes era la\n  infrestructura de un proyecto.
**********_ ahora
+++++++++++[#A7E79E] Se almacena la información en la nube.
************_ .
+++++++++++++[#8AF6D5] Significa que tu información esta en el servidor de\n grandes corporaciones.
+++++++++++[#A7E79E] Pagar por consumo.
+++++++++++[#A7E79E] Facilita la vida.
+++++++++++[#A7E79E] Metodologias agiles.
+++++++[#E0A893] Inconvenientes y como se solventan.
********_ .
+++++++++[#8AF6D5] Clientes que no desean que su información quede\n en manos de un tercero.
+++++++++[#8AF6D5] Google se rige por las leyes de Estados Unidos.
+++++++++[#8AF6D5] Seguridad de los datos.
@endmindmap
```

## Aplicación de la ingeniería de software 
```plantuml
@startmindmap
+[#9EAEE7] Aplicación de la\n ingenieria de software.
**_ . 
+++[#E49EE7] Existen empresas cuya identidad.
****_ son
+++++[#E79EB1] Conocimiento.
+++++[#E79EB1] Innovación.
+++++[#E79EB1] Calidad.
+++++[#E79EB1] Especialización.
+++[#E49EE7] Enfoque metodológico.
****_ como
+++++[#E79EB1] Generar documentos hasta hacer recolección de requisitos\n para que se convierta en un plan de proyecto.
+++++[#E79EB1] Visión global.
+++++[#E79EB1] Entender los procesos.
****_ .
+++++[#E79EB1] Pasos.
******_ .
+++++++[#E49EE7] Saber las necesidades del cliente.
+++++++[#E49EE7] Diseño fuinciona.
+++++++[#E49EE7] Analisis tecnico.
+++[#E49EE7] Existen tres fases.
****_ como
+++++[#E79EB1] Fase de analisis.
+++++[#E79EB1] Fase de construcción.
+++++[#E79EB1] Fase de prueba.
@endmindmap
```




